<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class KalkulasiParkiran {
		const BIAYA_MOTOR_AWAL = 2000;
		const BIAYA_MOBIL_AWAL = 5000;
		const BIAYA_PERJAM_MOTOR = 500;
		const BIAYA_PERJAM_MOBIL = 1000;
		
		public function calculating($biayaAwal, $jam_awal, $jam_akhir, $biayaPerJam)
        {		
			$jam = $this->calculate_hour($jam_awal, $jam_akhir);
			$biaya = $biayaAwal;
			$jam = intval($jam) - 1;
			if($jam > 0){
				$biaya = $biaya + ($biayaPerJam * $jam);
			}
			
			return $biaya;
        }
		
		public function kalkulasiBiaya($jenis_kendaraan, $jam_awal, $jam_akhir){
			if ($jenis_kendaraan == 1){ //mobil
				$biaya = $this->calculating(self::BIAYA_MOBIL_AWAL, $jam_awal, $jam_akhir, self::BIAYA_PERJAM_MOBIL);
			}else if($jenis_kendaraan == 2){ //motor
				$biaya = $this->calculating(self::BIAYA_MOTOR_AWAL, $jam_awal, $jam_akhir, self::BIAYA_PERJAM_MOTOR);
			}else{
				$biaya = 0;
			}
			
			return $biaya;
		}
		
		
		private function calculate_hour($start_date, $end_date)
        {		
			$date1 = new DateTime($start_date);
			$date2 = new DateTime($end_date);

			$diff = $date2->diff($date1);

			$hours = $diff->h;
			$hours = $hours + ($diff->days*24);

			return $hours;
        }
	}
?>