<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class TimeCalculate {

		public function calculate_hour($start_date, $end_date)
        {		
			$date1 = new DateTime($start_date);
			$date2 = new DateTime($end_date);

			$diff = $date2->diff($date1);

			$hours = $diff->h;
			$hours = $hours + ($diff->days*24);

			return $hours;
        }
	}
?>