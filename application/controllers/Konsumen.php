<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konsumen extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('m_konsumen');
	}
	
	public function index()
	{
		$d['konsumen'] = $this->m_konsumen->getAllKonsumen();
		$d['v'] = 'konsumen/daftar_konsumen';
		$this->load->view('template',$d);
	}
		
	public function getDropdown(){
		$this->load->model('m_dropdown');
		$d['kendaraan'] = $this->m_dropdown->getKendaraan();
		$d['jenis_kelamin'] = $this->m_dropdown->getJenisKelamin();
		return $d;
	}
	
	public function halaman_tambah(){	
		$d['dropdown'] = $this->getDropdown();
		$d['v'] = 'konsumen/tambah_konsumen';
		$this->load->view('template',$d);
	}
	
	public function tambah(){
		$query = $this->m_konsumen->insert();
		
		if($query){
			redirect('Parkiran');
		}else{
			$d['v'] = 'konsumen/tambah_konsumen';
			$d['message'] = 'tambah data gagal'; 
			$this->load->view('template',$d);
		}
	}
	
	public function halaman_rubah($user_id){
			$d['dropdown'] = $this->getDropdown();
			$d['v'] = 'konsumen/tambah_konsumen';
			$d['data'] = $this->m_konsumen->getKonsumenData($user_id);
			$this->load->view('template',$d);
	}
	
	public function rubah($user_id){
		$query = $this->m_konsumen->edit($user_id);
		
		if($query){
			redirect('Parkiran');
		}else{
			$d['v'] = 'konsumen/tambah_konsumen';
			$d['message'] = 'rubah data gagal'; 
			$this->load->view('template',$d);
		}
	}
	
	public function hapus($user_id){
		$query = $this->m_konsumen->delete_data($user_id);
		
		if($query){
			redirect('Parkiran');
		}else{
			$d['v'] = 'konsumen/tambah_konsumen';
			$d['message'] = 'rubah data gagal'; 
			$this->load->view('template',$d);
		}
	}
}
