<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('KalkulasiParkiran');
		$this->load->model('m_transaksi');
		$this->load->model('m_dropdown');
	}
	
	public function index()
	{	
		$d['transaksi'] = $this->m_transaksi->getAllTransaksi();
		
		foreach($d['transaksi']->result() as $row){
			$tanggal = date("d-M-Y",strtotime($row->tgl_masuk));
			$waktu_masuk = $this->format_time($row->waktu_masuk);
			
			
			$waktu_keluar = $this->format_time($row->waktu_keluar);			
			
			if($waktu_keluar != null){
				$biaya = $this->kalkulasiparkiran->kalkulasiBiaya($row->id_jenis_kendaraan, $row->waktu_masuk, $row->waktu_keluar);			
			}else{
				$biaya = "-";
			}
			
			$row->tgl_masuk = $tanggal;
			$row->biaya = $biaya;
			$row->waktu_masuk = $waktu_masuk;
			$row->waktu_keluar = $waktu_keluar;
		}
				
		$d['v'] = 'Transaksi/daftar_transaksi';
		$this->load->view('template',$d);
	}
	
	public function format_time($waktu){
		if ($waktu === null){
			$waktu_formatted = $waktu;
		}else{
			$waktu_formatted = date("H:i",strtotime($waktu));
		}
		
		return $waktu_formatted;
	}
	
	private function setting_validation(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('waktu_masuk','Masuk','required|trim|min_length[5]|max_length[5]|callback_validate_time');
		
		$waktu_keluar = $this->input->post('waktu_keluar');
		if(!empty($waktu_keluar)){
			$this->form_validation->set_rules('waktu_keluar','Keluar','trim|max_length[5]|callback_validate_time');
		}
	}
	
	private function fail_validation(){
		$d['no_polisi'] = $this->m_dropdown->getNoPolisi();
		$d['v'] = 'transaksi/form_transaksi';
		$this->load->view('template',$d);
	}
	
	private function fail_query($message){
		$d['v'] = 'konsumen/tambah_konsumen';
		$d['message'] = $message; 
		$this->load->view('template',$d);
	}
	
	public function transaksi_get_konsumen_by_no_polisi(){
		$this->load->model('m_konsumen');
		$no_polisi = $this->input->post('no_polisi',TRUE);
		$row_data = $this->m_konsumen->getKonsumenbyNoPolisi($no_polisi)->row();
		echo json_encode($row_data);
	}
	
	public function validate_time($str){
		if (strrchr($str,":")) {
			list($hh, $mm) = explode(':', $str);
			if (!is_numeric($hh) || !is_numeric($mm)){
				$this->form_validation->set_message('validate_time', 'Format waktu salah, harusnya HH:MM');
				return FALSE;
			}elseif ((int) $hh > 24 || (int) $mm > 59){
				$this->form_validation->set_message('validate_time', 'Pengisian Jam / menit Salah');
				return FALSE;
			}				
			return TRUE;
		}else{
			$this->form_validation->set_message('validate_time', 'Format waktu salah, harusnya HH:MM');
			return FALSE;
		}   
	}
	
	public function calculateBiaya(){
		$jenis_kendaraan = $this->input->post('jenis');
		$masuk = $this->input->post('masuk');
		$keluar = $this->input->post('keluar');
		$biaya = $this->kalkulasiparkiran->kalkulasiBiaya($jenis_kendaraan, $masuk, $keluar);		
		echo $biaya;
	}
		
	public function halaman_tambah(){
		$d['no_polisi'] = $this->m_dropdown->getNoPolisi();
		$d['v'] = 'transaksi/form_transaksi';
		$this->load->view('template',$d);
	}
	
	public function tambah(){
		$this->setting_validation();
		if($this->form_validation->run() == FALSE){
			$this->fail_validation();
		}else{
			$query = $this->m_transaksi->insert();
		
			if($query){
				redirect('Transaksi');
			}else{
				$this->fail_query("Terjadi kesalahan di server, tambah data gagal.");
			}
		}
	}
	
	public function halaman_rubah($ID){
		$d['no_polisi'] = $this->m_dropdown->getNoPolisi();
		$d['data'] = $this->m_transaksi->getTransaksiUser($ID);
		
		//ubah dulu waktunya agar hh:mm
		$d['data']->row()->waktu_masuk = $this->format_time($d['data']->row()->waktu_masuk);
		$d['data']->row()->waktu_keluar = $this->format_time($d['data']->row()->waktu_keluar);
		
		$d['v'] = 'transaksi/form_transaksi';
		$this->load->view('template',$d);
	}
	
	public function rubah($ID){
		$this->setting_validation();		
		if($this->form_validation->run() == FALSE){
			$this->fail_validation();
		}else{
			$query = $this->m_transaksi->edit($ID);
		
			if($query){
				redirect('Transaksi');
			}else{
				$this->fail_query("Terjadi kesalahan di server, rubah data gagal.");
			}
		}
	}
}
