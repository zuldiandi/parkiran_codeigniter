<?php

class m_dropdown extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	public function getKendaraan(){
		$this->db->select('ID, jenis_kendaraan');
		$this->db->from('jenis_kendaraan');
		return $this->db->get();
	}
	
	public function getJenisKelamin(){
		$this->db->select('ID, jenis_kelamin');
		$this->db->from('jenis_kelamin');
		return $this->db->get();
	}
	
	public function getNoPolisi(){
		$this->db->select('no_polisi');
		$this->db->from('konsumen');
		return $this->db->get();
	}
}