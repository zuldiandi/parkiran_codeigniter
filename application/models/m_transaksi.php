<?php

class m_transaksi extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	public function getAllTransaksi(){
		$this->db->select('t.ID, k.konsumen, t.no_polisi, t.tgl_masuk, t.waktu_masuk, t.waktu_keluar, k.id_jenis_kendaraan');
						   //TIMESTAMPDIFF(HOUR, waktu_masuk, waktu_keluar) as perbedaan_jam'); if you want to use it at DB.
						   //now didn't use this since when transaction happen you want JQUERY to calculate.
		$this->db->from('transaksi as t');
		$this->db->join('konsumen as k','k.no_polisi = t.no_polisi','left');
		return $this->db->get();
	}
	
	public function getTransaksiUser($ID){
		$this->db->select('t.ID, k.konsumen, t.no_polisi, t.tgl_masuk, t.waktu_masuk, t.waktu_keluar, k.id_jenis_kendaraan');
		$this->db->from('transaksi as t');
		$this->db->join('konsumen as k','k.no_polisi = t.no_polisi','left');
		$this->db->where('t.ID',$ID);
		return $this->db->get();
	}
	
	public function insert(){
		$polisi = $this->input->post('no_polisi');
		$tanggal = $this->input->post('tanggal');
		$waktu_masuk = $this->security->xss_clean($this->input->post('waktu_masuk'));
		//agar insert null
		$waktu_keluar = ($this->input->post('waktu_keluar')!=FALSE) ? $this->security->xss_clean(trim($this->input->post('waktu_keluar'))) : NULL;
		
		var_dump($waktu_keluar);
		exit;
		
		$data_input = array(
			'no_polisi' => $polisi,
			'tgl_masuk' => $tanggal,
			'waktu_masuk' => $waktu_masuk,
			'waktu_keluar' => $waktu_keluar
		);
		
		return $this->db->insert('transaksi',$data_input);
	}
	
	public function edit($ID){
		$polisi = $this->input->post('no_polisi');
		$tanggal = $this->input->post('tanggal');
		$waktu_masuk = $this->security->xss_clean(trim($this->input->post('waktu_masuk')));
		
		//agar insert null
		$waktu_keluar = ($this->input->post('waktu_keluar')!=FALSE) ? $this->security->xss_clean(trim($this->input->post('waktu_keluar'))) : NULL;
				
		$data_input = array(
			'no_polisi' => $polisi,
			'tgl_masuk' => $tanggal,
			'waktu_masuk' => $waktu_masuk,
			'waktu_keluar' => $waktu_keluar
		);
		
		return $this->db->update('transaksi',$data_input,'ID='.$ID);
	}
}