<?php

class m_konsumen extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	
	public function getAllKonsumen(){
		$this->db->select('k.ID, k.konsumen, jd.jenis_kendaraan, k.no_polisi, k.tgl_lahir, jk.jenis_kelamin, k.no_hp');
		$this->db->from('konsumen as k');
		$this->db->join('jenis_kelamin as jk','k.id_jenis_kelamin = jk.ID');
		$this->db->join('jenis_kendaraan as jd','k.id_jenis_kendaraan = jd.ID');
		return $this->db->get();
	}
	
	public function getKonsumenData($user_ID){
		$this->db->select('ID, konsumen, id_jenis_kendaraan, no_polisi, tgl_lahir, id_jenis_kelamin, no_hp');
		$this->db->from('konsumen');
		$this->db->where('ID',$user_ID);
		return $this->db->get();
	}
	
	public function getKonsumenbyNoPolisi($no_polisi){
		$this->db->select('konsumen, id_jenis_kendaraan');
		$this->db->from('konsumen');
		$this->db->where('no_polisi',$no_polisi);
		return $this->db->get();
	}
	
	public function insert(){
		$nama = $this->security->xss_clean(trim($this->input->post('konsumen')));
		$jenis_kendaraan = $this->input->post('kendaraan');
		$polisi = $this->security->xss_clean(trim($this->input->post('no_polisi')));
		$tanggal = $this->input->post('tanggal');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$no_hp = $this->input->post('no_hp');
		
		$data_input = array(
			'konsumen' => $nama,
			'id_jenis_kendaraan' => $jenis_kendaraan,
			'no_polisi' => $polisi,
			'tgl_lahir' => $tanggal,
			'id_jenis_kelamin' => $jenis_kelamin,
			'no_hp' => $no_hp
		);
		
		return $this->db->insert('konsumen',$data_input);
	}
	
	public function edit($user_id){
		$nama = $this->security->xss_clean(trim($this->input->post('konsumen')));
		$jenis_kendaraan = $this->input->post('kendaraan');
		$polisi = $this->security->xss_clean(trim($this->input->post('no_polisi')));
		$tanggal = $this->input->post('tanggal');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$no_hp = $this->input->post('no_hp');
		
		$data_input = array(
			'konsumen' => $nama,
			'id_jenis_kendaraan' => $jenis_kendaraan,
			'no_polisi' => $polisi,
			'tgl_lahir' => $tanggal,
			'id_jenis_kelamin' => $jenis_kelamin,
			'no_hp' => $no_hp
		);
		
		return $this->db->update('konsumen',$data_input,'ID='.$user_id);
	}
	
	public function delete_data($user_ID){
		return $this->db->delete('konsumen',array('ID'=>$user_ID));
	}
}