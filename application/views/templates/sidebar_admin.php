<div id="sidebar-container" class="bd-sidebar">
	<div class="sidebar-expanded">
		<ul class="list-group">
			<li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
				<small>Transaksi</small>
			</li>
		
			<a href="#submenu1" data-toggle="collapse" aria-expanded="false" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
				<div class="d-flex w-100 justify-content-start align-items-center">
					<span class="far fa-user fa-fw mr-3"></span>
					<span class="menu-collapsed">User</span>
					<span class="fas fa-angle-down ml-auto"></span>
				</div>
			</a>
			
			<div id='submenu1' class="collapse sidebar-submenu">
				<a href="<?php echo site_url('User/halaman_user'); ?>" class="list-group-item list-group-item-action bg-dark text-white">
						<span class="menu-collapsed">Daftar User</span>
				</a>
			</div>
			
			<a href="#submenu2" data-toggle="collapse" aria-expanded="false" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
				<div class="d-flex w-100 justify-content-start align-items-center">
					<span class="far fa-address-book fa-fw mr-3"></span>
					<span class="menu-collapsed">Ticketing</span>
					<span class="fas fa-angle-down ml-auto"></span>
				</div>
			</a>
			
			<div id='submenu2' class="collapse sidebar-submenu">
				<a href="<?php echo site_url('Laporan/daftar_laporan');?>" class="list-group-item list-group-item-action bg-dark text-white">
					<span class="menu-collapsed">Daftar Ticket</span>
				</a>
			</div>
		</ul>	
	</div>
</div>
