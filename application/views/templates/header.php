<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sistem Informasi Parkiran</title>
</head>
<body>
	<nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="<?= site_url('Konsumen');?>">
						<i class="fa fa-user mr-2"></i>Daftar Konsumen
					</a>
				</li>
				
				<li class="nav-item active">
					<a class="nav-link" href="<?= site_url('Transaksi');?>">
						<i class="fa fa-book mr-2"></i>Daftar Transaksi
					</a>
				</li>
				
				
			</ul>
		</div>
	</nav>