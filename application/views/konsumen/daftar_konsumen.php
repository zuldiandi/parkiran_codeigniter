<main class="container-fluid py-md-3 bd-content" role="main">
	<b>DAFTAR KONSUMEN</b>
	<br/>
	
	<!-- Button  -->
	<div class="py-md-3">
		<button class="btn btn-success active" onclick="location.href='<?php echo site_url('konsumen/halaman_tambah/'); ?>'">
		<i class="fa fa-plus mr-2"></i>Tambah</button>
	</div>

	<!-- list perbaikan -->
	<div class="row">
		<div class="col py-md-3">
			<table class="table table-bordered table-responsive-sm">
				<thead class="thead-dark text-center">
					<tr>
						<th>Konsumen</th>
						<th>Jenis Kendaraan</th>
						<th>No Polisi</th>
						<th>Tanggal Lahir</th>
						<th>Jenis Kelamin</th>
						<th>No. HP</th>
						<th colspan=2>Aksi</th>
					</tr>
				</thead>
				
				<tbody>
					<?php 
					if($konsumen->num_rows() > 0){
						foreach($konsumen->result() as $row){
							echo '<tr>';
							echo '<td>'.$row->konsumen.'</td>';
							echo '<td>'.$row->jenis_kendaraan.'</td>';
							echo '<td>'.$row->no_polisi.'</td>';
							echo '<td>'.$row->tgl_lahir.'</td>';	
							echo '<td>'.$row->jenis_kelamin.'</td>';
							echo '<td>'.$row->no_hp.'</td>';
					?>
						<td class="text-center">
							<button class="btn btn-success active" onclick="location.href='<?php echo site_url('konsumen/halaman_rubah/').$row->ID; ?>'">
								<i class="fa fa-pen mr-2"></i>Rubah
							</button>
						</td>
						
						<td class="text-center">
							<button class="btn btn-danger active" onclick="location.href='<?php echo site_url('konsumen/hapus/').$row->ID; ?>'">
								<i class="fa fa-trash mr-2"></i>Hapus
							</button>
						</td>
					<?php
							echo '</tr>';
						}
					}else{
						echo '<tr colspan=8>';
						echo '<td>belum ada data</td>';
						echo '</tr>';
					}
					?>
							
				</tbody>
			</table>
		</div>
	</div>
</main>