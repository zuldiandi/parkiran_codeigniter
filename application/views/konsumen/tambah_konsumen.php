<main class="container-fluid py-md-3 bd-content" role="main">
	<b>PENAMBAHAN USER</b><br/>
	<!-- this below need to change to use form_validation function of CI -->
	<h6 class="h6 text-danger"><?php if (isset($message)){echo $message;} ?></h6>
	
	<form action="<?php 
					if(isset($data)){
						echo site_url('konsumen/rubah/'.$data->row()->ID);
					}else{
						echo site_url('konsumen/tambah'); 
					}
					?>" method="POST" class="py-md-3">
		<div class="form-group">
			<label for="txt_username">Nama Konsumen</label>
			<input class="form-control" type="text" id="txt_username" name="konsumen" value="<?php 
					if(isset($data)){
						echo $data->row()->konsumen;
					}else{
						echo set_value('username');
					}
				?>" required></input>
		</div>
		
		<div class="form-group">
			<label for="dropdown_departement">Jenis Kendaraan</label>
			<select class="form-control" id="dropdown_departement" name="kendaraan" required>
				<option value=""> </option>
				<?php 
					foreach($dropdown['kendaraan']->result() as $row)
					{
						//                                   jika variabel data ada datanya maka ini adalah operasi rubah, 
						//                                   ketika ini rubah bandingkan ID agar terpilih ke data yang benar dropdownnya
						echo '<option value="'.$row->ID.'"'.(isset($data)?($row->ID == $data->row()->id_jenis_kendaraan?'selected':''):'').'>'.$row->jenis_kendaraan.'</option>';
					}
				?>
			</select>
		</div>
		
		<div class="form-group">
			<label for="txt_username">No. Polisi</label>
			<input class="form-control" type="text" id="txt_username" name="no_polisi" value="<?php 
					if(isset($data)){
						echo $data->row()->no_polisi;
					}else{
						echo set_value('no_polisi');
					}
				?>" required></input>
		</div>
		
		<div class="form-group">
			<label for="dt_tanggal">Tanggal Lahir</label>
			<input class="form-control" type="date" name="tanggal" id="dt_tanggal" value="<?php if (isset($data)){echo $data->row()->tgl_lahir;} ?>" required></input>
		</div>
		
		<div class="form-group">		
			<label for="dropdown_akses">Jenis Kelamin</label>
			<select class="form-control" id="dropdown_akses" name="jenis_kelamin" required>			
				<?php 
					foreach($dropdown['jenis_kelamin']->result() as $row)
					{
						echo '<option value="'.$row->ID.'"'.(isset($data)?($row->ID == $data->row()->id_jenis_kelamin?'selected':''):'').'>'.$row->jenis_kelamin.'</option>';
					}
				?>
			</select>
		</div>
		
		<div class="form-group">
			<label for="txt_username">No. HP</label>
			<input class="form-control" type="text" id="txt_username" name="no_hp" value="<?php 
					if(isset($data)){
						echo $data->row()->no_hp;
					}else{
						echo set_value('no_hp');
					}
				?>" required></input>
		</div>
			
		<div class="d-flex justify-content-center">
			<div class="form-group">
				<?php
					//ketika ada data kiriman untuk edit
					if(isset($data)){
						echo '<input type="submit" class="btn btn-success" value="Ubah" />';
					}else{
						echo '<input type="submit" class="btn btn-success" value="Selesai" />';
					}?>
			</div>
		</div>
    </form>
</main>