<main class="container-fluid py-md-3 bd-content" role="main">
	<b>PENAMBAHAN TRANSAKSI</b><br/>
	<form action="<?php 
					if(isset($data)){
						echo site_url('transaksi/rubah/'.$data->row()->ID);
					}else{
						echo site_url('transaksi/tambah'); 
					}
					?>" method="POST" class="py-md-3">
	<div class="form-row align-items-center">
		<div class="col">
			<div class="form-group row">
				<label for="dropdown_polisi" class="col-sm-2 col-form-label">No. Polisi</label>
				<div class="col-sm-10">
					<select class="form-control" id="polisi" name="no_polisi" required>
					<option value="" hidden> </option>
						<?php 
							foreach($no_polisi->result() as $row)
							{
								//                                   jika variabel data ada datanya maka ini adalah operasi rubah, 
								//                                   ketika ini rubah bandingkan ID agar terpilih ke data yang benar dropdownnya
								echo '<option value="'.$row->no_polisi.'"'.(isset($data)?($row->no_polisi == $data->row()->no_polisi?'selected':''):'').'>'.$row->no_polisi.'</option>';
							}
						?>
					</select>
				</div>
			</div>
		
			<div class="form-group row">
				<label for="txt_nama" class="col-sm-2 col-form-label">Nama Konsumen</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" id="nama" name="txt_nama" readonly></input>
				</div>
			</div>
			
				<div class="col-sm-10">
					<input class="form-control" type="text" id="kendaraan" name="txt_id" hidden></input>
				</div>
			
			<div class="form-group row">
				<label for="dt_tanggal" class="col-sm-2 col-form-label">Tanggal Transaksi</label>
				<div class="col-sm-10">
					<input class="form-control" type="date" name="tanggal" id="dt_tanggal" value="<?php 
						if(isset($data)){
							echo $data->row()->tgl_masuk;
						}else{
							echo set_value('tanggal');
						}
						?>">
					</input>
				</div>
			</div>		
			
			<!-- this too so it input like time and verify too -->
			<div class="form-group row">
				<label for="txt_username" class="col-sm-2 col-form-label">Waktu Masuk</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" id="txt_masuk" name="waktu_masuk" value="<?php 
						if(isset($data)){
							echo $data->row()->waktu_masuk;
						}else{
							echo set_value('waktu_masuk');
						}
						?>" placeholder="Masukan dengan format HH:mm, co: 13:00">
					</input>
					<div class="h6 text-danger"><?php echo form_error('waktu_masuk'); ?></div>
				</div>
			</div>
			
		
			<div class="form-group row">
				<label for="txt_username" class="col-sm-2 col-form-label">Waktu Keluar</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" id="txt_keluar" name="waktu_keluar" value="<?php 
						if(isset($data)){
							echo $data->row()->waktu_keluar;
						}else{
							echo set_value('waktu_keluar');
						}
					?>" placeholder="Masukan dengan format HH:mm, co: 15:00">
					</input>
					<div class="h6 text-danger"><?php echo form_error('waktu_keluar'); ?></div>
				</div>
			</div>
		
			<div class="form-group row">
				<label for="txt_username" class="col-sm-2 col-form-label">Biaya</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" id="txt_biaya" name="biaya" disabled></input>
				</div>
			</div>
		</div>

		<div class="col-md-2">
			<div class="d-flex flex-md-column justify-content-around">
				<div class="form-group">
					<?php
						//ketika ada data kiriman untuk edit
						if(isset($data)){
							echo '<input type="submit" class="btn btn-success btn-block" value="Ubah" />';
						}else{
							echo '<input type="submit" class="btn btn-success btn-block" value="Selesai" />';
						}?>
				</div>
			
				<div class="form-group sm-2">
					<button class="btn btn-success btn-block">Cancel</button>
				</div>
			</div>
		</div>
	</div>
    </form>
	
<script type="text/javascript">
$(document).ready(getKonsumen);
$(document).ready(function(){
	$('#polisi').on("change focus",getKonsumen);
	
	$('#txt_masuk,#txt_keluar').on('focusout',function(){
		kalkulasi();
	});
});

//untuk mengambil nama konsumen.
function getKonsumen(){
	if($('#polisi').val().length != 0){
		var polisi = $('#polisi').val();
		$.ajax({
			url: "<?php echo site_url('Transaksi/transaksi_get_konsumen_by_no_polisi'); ?>",
			method : "POST",
			data : {no_polisi:polisi},
			dataType : "json",
			async : true,
			success : function(data){
				if(data != null){
					$('#nama').val(data.konsumen);
					$('#kendaraan').val(data.id_jenis_kendaraan);
					kalkulasi();
				}
			}
		});
	}
}

//untuk kalkulasi biaya
function kalkulasi(){
	if(($('#txt_masuk').val().length >= 5) && ($('#txt_keluar').val().length >= 5) && ($('#kendaraan').val().length != 0)) {
		var masuk = $('#txt_masuk').val();
		var keluar = $('#txt_keluar').val();
		var kendaraan = $('#kendaraan').val();
		$.ajax({
			url: "<?php echo site_url('Transaksi/calculateBiaya'); ?>",
			method : "POST",
			data : {jenis:kendaraan, masuk:masuk, keluar:keluar},
			async : true,
			success : function(data){
				$('#txt_biaya').val(data);
			}
		});
	}
}
</script>
</main>