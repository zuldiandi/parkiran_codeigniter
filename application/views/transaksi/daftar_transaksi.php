<main class="container-fluid py-md-3 bd-content" role="main">
	<b>DAFTAR TRANSAKSI</b>
	<br/>
	<!-- Button  -->
	<div class="py-md-3">
		<button class="btn btn-success active" onclick="location.href='<?php echo site_url('Transaksi/halaman_tambah');?>'">
		<i class="fa fa-plus mr-2"></i>Tambah</button>
	</div>

	<!-- list perbaikan -->
	<div class="row">
		<div class="col py-md-3">
			<table class="table table-bordered table-responsive-lg" name="tables" id="tables">
				<thead class="thead-dark text-center">
					<tr>
						<th>Konsumen</th>
						<th>No Polisi</th>
						<th>Tanggal</th>
						<th>Masuk</th>
						<th>Keluar</th>
						<th>Biaya</th>
						<th colspan=2>Aksi</th>
					</tr>
				</thead>
				
				<tbody>
					<?php 
					if($transaksi->num_rows() > 0){
						foreach($transaksi->result() as $row){
							echo '<tr>';
							echo '<td>'.$row->konsumen.'</td>';
							echo '<td>'.$row->no_polisi.'</td>';
							echo '<td>'.$row->tgl_masuk.'</td>';
							echo '<td class="text-center">'.$row->waktu_masuk.'</td>';	
							echo '<td class="text-center">'.$row->waktu_keluar.'</td>';
							echo '<td class="text-right">'.$row->biaya.'</td>';
					?>
						<td class="text-center">
							<button class="btn btn-info active" id="btn_detail" data-toggle="modal" data-target="#modalDetail"
								data-konsumen = "<?=$row->konsumen?>"
								data-polisi ="<?=$row->no_polisi?>"
								data-tanggal ="<?=$row->tgl_masuk?>"
								data-waktu_masuk ="<?=$row->waktu_masuk?>"
								data-waktu_keluar ="<?=$row->waktu_keluar?>"
								data-biaya ="<?=$row->biaya?>">
								<i class="fa fa-eye mr-2"></i>Detail
							</button>
						</td>
						
						<td class="text-center">
							<button class="btn btn-success active" onclick="location.href='<?php echo site_url('Transaksi/halaman_rubah/'.$row->ID); ?>'">
								<i class="fa fa-pen mr-2"></i>Edit
							</button>
						</td>
					<?php
							echo '</tr>';
						}
					}else{
						echo '<tr colspan=9>';
						echo '<td>belum ada data</td>';
						echo '</tr>';
					}
					?>
							
				</tbody>
			</table>
		</div>
	</div>
	
	<div id="modalDetail" class="modal fade" role="dialog" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered" role="document">
	
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Detail Transaksi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				
				<div class="modal-body table-responsive" id="showdata">
					<div class="row justify-content-md-center">
						<div class="col">Konsumen</div>
						<div class="col col-md-auto">:</div>
						<div class="col" id="modal_konsumen"></div>
					</div>
					
					<div class="row justify-content-md-center">
						<div class="col">No. Polisi</div>
						<div class="col col-md-auto">:</div>
						<div class="col" id="modal_polisi"></div>
					</div>
					
					<div class="row justify-content-md-center">
						<div class="col">Tgl. Transaksi</div>
						<div class="col col-md-auto">:</div>
						<div class="col" id="modal_tanggal"></div>
					</div>
					
					<div class="row justify-content-md-center">
						<div class="col">Waktu Masuk</div>
						<div class="col col-md-auto">:</div>
						<div class="col" id="modal_masuk"></div>
					</div>
					
					<div class="row justify-content-md-center">
						<div class="col">Waktu Keluar</div>
						<div class="col col-md-auto">:</div>
						<div class="col" id="modal_keluar"></div>
					</div>
					
					<div class="row justify-content-md-center">
						<div class="col">Biaya</div>
						<div class="col col-md-auto">:</div>
						<div class="col" id="modal_biaya"></div>
					</div>
				</div>
			
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
$(document).ready(function(){
	$(document).on('click','#btn_detail', function(){
		$('#modal_konsumen').text($(this).data("konsumen"));
		$('#modal_polisi').text($(this).data("polisi"));
		$('#modal_tanggal').text($(this).data("tanggal"));
		$('#modal_masuk').text($(this).data("waktu_masuk"));
		$('#modal_keluar').text($(this).data("waktu_keluar"));
		$('#modal_biaya').text($(this).data("biaya"));
	})
});
</script>	
</main>